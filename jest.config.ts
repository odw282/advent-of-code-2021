import type { Config } from "@jest/types";

// Sync object
const config: Config.InitialOptions = {
  silent: false,
  verbose: false,
  preset: "ts-jest",
  testEnvironment: "node",
  reporters: [
    "./utils/jest-include-console-log-reporter.js",
    "@jest/reporters/build/SummaryReporter",
  ],
};

export default config
