import * as fs from "fs";

/**
 * Reads a file into and array, separated by and trimmed of last line
 * @param path
 * @param separator
 * @param trim
 */
export function readFileIntoArray(path: string, separator = "\n", trim = true) {
    const list = fs.readFileSync(path).toString().split(separator);
  return trim ? list.slice(0, list.length - 1) : list;
}
