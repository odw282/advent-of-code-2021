import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import { countFlashes } from "./index";

const testOcto = readFileIntoArray(path.join(__dirname, "./test.txt")).map(
  (value) => value.split("").map(i => parseInt(i))
);

const octo = readFileIntoArray(path.join(__dirname, "./input.txt")).map(
  (value) => value.split("").map(i => parseInt(i))
);

describe("--- Day 11: Dumbo Octopus ---", () => {

    test("Test Case A", () => {
      const { total: answer} = countFlashes(testOcto, 100);
      expect(answer).toEqual(1656);
    });

    test("How many total flashes are there after 100 steps?", () => {
      const { total: answer} = countFlashes(octo, 100);
      console.info("A:", answer);
    });

    test("Test Case B", () => {
      const { steps: answer} = countFlashes(testOcto, 200);
      expect(answer).toEqual(195);
    });

    it("Solution B", () => {
      const { steps: answer} = countFlashes(octo, 400);
      console.info("B:", answer);
    });

});
