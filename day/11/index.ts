
// adjacent mapping
const adj = [
  [-1, -1],
  [ 0, -1],
  [ 1, -1],
  [-1,  0],
  //[0, 0],
  [ 1,  0],
  [-1,  1],
  [ 0,  1],
  [ 1,  1],
]

function print(grid: number[][], step: number = 0, flashes = 0) {
  console.log(`Step: ${step}, Flashes: ${flashes} \n${grid.join("\n")}`);
}

/*
 * Stub
 */
function countFlashes(input: number[][], nrSteps = 10) {
  let steps = 0;
  let total = 0;

  // copy grid
  const grid = input.map(row => row.map(i => i));

  // Run a step
  const step = (grid: number[][]) => {
    // Flash all octopus when level > 9
    const flash = (grid: number[][]) => {

      const doFlash = (x: number, y: number) => {
        // Need to flash
        if (grid[y][x] > 9) {
          grid[y][x] = 0;
          nrFlashes++;
          flashed.add(`${x}_${y}`);
          // Add energy to adjacent
          for (let a = 0; a < adj.length; a++) {
            const ay = y + adj[a][1];
            const ax = x + adj[a][0];
            if (ay >= 0 && ay < grid.length && ax >= 0 && ax < grid[ay].length) {
              if (grid[ay][ax] !== 0) {
                grid[ay][ax]++;
              }
              doFlash(ax, ay);
            }
          }
        }
      }

      let nrFlashes = 0;
      for (let y = 0; y < grid.length; y++) {
        for (let x = 0; x < grid[y].length; x++) {
          doFlash(x, y);
        }
      }
      return nrFlashes;
    }


    // count step
    steps++;

    // Count total number of flashes
    let totalFlashes = 0;

    // list of positions that flashed this step
    const flashed = new Set<string>();

    // Increase energy level by one
    for (let y = 0; y < grid.length; y++) {
      for (let x = 0; x < grid[y].length; x++) {
        grid[y][x] += 1;
      }
    }

    // flash the grid;
    let nrFlashes = 0;

    //
    while ((nrFlashes = flash(grid)) > 0) {

      if (flashed.size === grid.length * grid[0].length && grid.flatMap(i => i).every(i => i === 0)) {
        //console.log('step', steps, 'nrFlashes', nrFlashes);
        print(grid, steps, total);
        nrSteps = steps;
      }

      //print(grid, steps, total);
      // add to total
      totalFlashes += nrFlashes;
    }


    return totalFlashes;
  }

  for (let i = 0; i < nrSteps; i++) {
    total += step(grid);
  }

  return {
    steps,
    total
  };
}

export {
  countFlashes
};
