export interface BingoInput {
  picks: number[];
  draws: number[];
  sheets: Array<number[][]>;
}

export interface BingoSubSystem extends BingoInput {
  hits: Array<number[][]>;
}

function buildSubSystem(lines: string[]): BingoSubSystem {
  // parse the input
  const input = lines.reduce(
    ({ picks, sheets }, line, index) => {
      if (index === 0) {
        picks = line.split(",").map((v) => parseInt(v));
      } else if (line === "") {
        sheets.push([]);
      } else {
        sheets[sheets.length - 1].push(
          line
            .split(/\s+/)
            .filter((n) => n !== "")
            .map((v) => parseInt(v))
        );
      }
      return {
        picks,
        sheets,
      };
    },
    {
      picks: [],
      sheets: [],
    }
  );

  // setup state
  const hits = [];
  for (let i = 0; i < input.sheets.length; i++) {
    hits.push([]);
    for (let colIndex = 0; colIndex < input.sheets[i][0].length; colIndex++) {
      hits[i].push([]);
      for (let rowIndex = 0; rowIndex < input.sheets[i].length; rowIndex++) {
        // @ts-ignore
        hits[i][colIndex][rowIndex] = 0;
      }
    }
  }
  return {
    ...input,
    hits,
    draws: [],
  };
}

/**
 * Stub
 */
function bingo(system: BingoSubSystem) {
  let draw = 0;
  let winner = system.picks.reduce((winner, pick, index) => {
    if (winner !== 0) {
      draw = system.draws[system.draws.length - 1];
      return winner;
    }
    system.draws.push(pick);
    // for each sheet set if pick was found
    for (let sheetIndex = 0; sheetIndex < system.sheets.length; sheetIndex++) {
      const sheet = system.sheets[sheetIndex];
      for (let rowIndex = 0; rowIndex < sheet.length; rowIndex++) {
        for (let colIndex = 0; colIndex < sheet[rowIndex].length; colIndex++) {
          // this sheet has this number
          if (sheet[rowIndex][colIndex] === pick) {
            system.hits[sheetIndex][rowIndex][colIndex] = 1;
          }
        }
      }
    }

    // After each pick check for bingo
    for (let hitIndex = 0; hitIndex < system.hits.length; hitIndex++) {
      const hits = system.hits[hitIndex];

      for (let rowIndex = 0; rowIndex < hits.length; rowIndex++) {
        // Check for bingo in row
        if (hits[rowIndex].every((hit) => hit === 1)) {
          winner = hitIndex;
        }

        // check for bingo in columns
        for (let colIndex = 0; colIndex < hits[0].length; colIndex++) {
          if (hits.map((row) => row[colIndex]).every((hit) => hit === 1)) {
            winner = hitIndex;
          }
        }
      }
    }
    return winner;
  }, 0);

  const sum = system.hits[winner].reduce(
    (totalSum, row, rowIndex) =>
      totalSum +
      row.reduce(
        (rowSum, hit, colIndex) =>
          rowSum + (hit === 0 ? system.sheets[winner][rowIndex][colIndex] : 0),
        0
      ),
    0
  );

  return sum * draw;
}

/**
 * Stub
 */
function lastBingo(system: BingoSubSystem) {
  const untilLast = true;
  let lastWinner = 0;
  let lastDraw = 0;
  let winners = new Set();
  let winningDraws = new Set();
  let draw = 0;
  let winner = system.picks.reduce((winner, pick, index) => {
    if (untilLast && winners.size === system.hits.length) {
      return lastWinner;
    } else if (untilLast && winner !== lastWinner) {
      lastWinner = winner;
      lastDraw = system.draws[system.draws.length - 1];
    } else if (!untilLast && winner !== 0) {
      return winner;
    }
    system.draws.push(pick);
    // for each sheet set if pick was found
    for (let sheetIndex = 0; sheetIndex < system.sheets.length; sheetIndex++) {
      const sheet = system.sheets[sheetIndex];
      for (let rowIndex = 0; rowIndex < sheet.length; rowIndex++) {
        for (let colIndex = 0; colIndex < sheet[rowIndex].length; colIndex++) {
          // this sheet has this number
          if (sheet[rowIndex][colIndex] === pick) {
            system.hits[sheetIndex][rowIndex][colIndex] = 1;
          }
        }
      }
    }

    // After each pick check for bingo
    for (let hitIndex = 0; hitIndex < system.hits.length; hitIndex++) {
      const hits = system.hits[hitIndex];

      for (let rowIndex = 0; rowIndex < hits.length; rowIndex++) {
        // Check for bingo in row
        if (hits[rowIndex].every((hit) => hit === 1)) {
          winner = hitIndex;
          winners.add(hitIndex);
          winningDraws.add(pick);
        }

        // check for bingo in columns
        for (let colIndex = 0; colIndex < hits[0].length; colIndex++) {
          if (hits.map((row) => row[colIndex]).every((hit) => hit === 1)) {
            winner = hitIndex;
            winners.add(hitIndex);
            winningDraws.add(pick);
          }
        }
      }
    }
    return winner;
  }, 0);

  if (untilLast) {
    const last = Array.from(winners.values()) as number[];
    const lastDraw = Array.from(winningDraws.values()) as number[];
    winner = last[last.length - 1];
    draw = lastDraw[lastDraw.length - 1];
  } else {
    draw = system.draws[system.draws.length - 1];
  }

  const sum = system.hits[winner].reduce(
    (totalSum, row, rowIndex) =>
      totalSum +
      row.reduce(
        (rowSum, hit, colIndex) =>
          rowSum + (hit === 0 ? system.sheets[winner][rowIndex][colIndex] : 0),
        0
      ),
    0
  );
  return sum * draw;
}

export { buildSubSystem, bingo, lastBingo };
