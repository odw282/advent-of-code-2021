import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import { bingo, buildSubSystem, lastBingo } from "./index";

const testLines = buildSubSystem(
  readFileIntoArray(path.join(__dirname, "./test.txt"))
);

const bingoSheets = buildSubSystem(
  readFileIntoArray(path.join(__dirname, "./input.txt"))
);

describe("--- Day 4: Giant Squid ---", () => {
  test("Test Case A", () => {
    const answer = bingo(testLines);
    expect(answer).toEqual(4512);
  });

  test("What will your final score be if you choose that board?", () => {
    const answer = bingo(bingoSheets);
    console.info("A:", answer);
  });

  test("Test Case B", () => {
    const answer = lastBingo(testLines);
    expect(answer).toEqual(1924);
  });

  it("Once it wins, what would its final score be?", () => {
    const answer = lastBingo(bingoSheets);
    console.info("B:", answer);
  });
});
