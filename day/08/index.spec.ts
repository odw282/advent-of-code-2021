import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import { countOutputs, decodeSignals } from "./index";

const decode = (line: string) => {
  const [signals, outputs] = line.split(" | ");
  return {
    signals: signals.split(" "),
    outputs: outputs.split(" "),
  }
};

const testSignals = readFileIntoArray(path.join(__dirname, "./test.txt")).map(decode);
const signals = readFileIntoArray(path.join(__dirname, "./input.txt")).map(decode);

describe("--- Day 8: Seven Segment Search ---", () => {

    test("Test Case A", () => {
      const answer = countOutputs(testSignals);
      expect(answer).toEqual(26);
    });

    test("In the output values, how many times do digits 1, 4, 7, or 8 appear?", () => {
      const answer = countOutputs(signals);
      console.info("A:", answer);
    });

    test("Test Case B", () => {
      const answer = decodeSignals(testSignals);
      expect(answer).toEqual(61229);
    });

    it("What do you get if you add up all of the output values?", () => {
      const answer = decodeSignals(signals);
      console.info("B:", answer);
    });

});
