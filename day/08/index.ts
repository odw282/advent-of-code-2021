export type Pattern = {
  signals: string[];
  outputs: string[];
};

/**
 * Maps Segment position to bit position
 */
const segmentToBit = new Map([
  ["a", 0],
  ["b", 1],
  ["c", 2],
  ["d", 3],
  ["e", 4],
  ["f", 5],
  ["g", 6],
]);

/**
 * Maps Segment position to value
 * @param segments
 */
function segmentsToValue(segments: string[]) {
  return segments.reduce(
    (total, segment) => total + Math.pow(2, segmentToBit.get(segment)),
    0
  );
}

/**
 * Create a map of segments to a value
 */
const values = new Map([
  [segmentsToValue(["a", "b", "c", "e", "f", "g"]), 0],
  [segmentsToValue(["c", "f"]), 1],
  [segmentsToValue(["a", "c", "d", "e", "g"]), 2],
  [segmentsToValue(["a", "c", "d", "f", "g"]), 3],
  [segmentsToValue(["b", "c", "d", "f"]), 4],
  [segmentsToValue(["a", "b", "d", "f", "g"]), 5],
  [segmentsToValue(["a", "b", "d", "f", "e", "g"]), 6],
  [segmentsToValue(["a", "c", "f"]), 7],
  [segmentsToValue(["a", "b", "c", "d", "e", "f", "g"]), 8],
  [segmentsToValue(["a", "b", "c", "d", "f", "g"]), 9],
]);

function decodeSignals(patterns: Pattern[]) {
  patterns.forEach((pattern) => decodeSignal(pattern));

  return patterns.reduce((total, pattern) => total + decodeSignal(pattern), 0);
}

function decodeSignal(pattern: Pattern) {
  const signals = pattern.signals.map((signal) => signal.split(""));
  const outputs = pattern.outputs.map((output) => output.split(""));

  // 1. signal length = 2
  const one = signals.find((signal) => signal.length === 2);
  // 7. signal length = 3
  const seven = signals.find((signal) => signal.length === 3);
  // 4. signal length = 4
  const four = signals.find((signal) => signal.length === 4);
  // 8. signal length = 7
  const eight = signals.find((signal) => signal.length === 7);
  // 0. 6. 9. signal length = 6
  const zeroSixNine = signals.filter((signal) => signal.length === 6);
  // 6.
  const sixIndex = zeroSixNine.findIndex(
    (signal) => !seven.every((segment) => signal.includes(segment))
  );
  const [six] = zeroSixNine.splice(sixIndex, 1);
  // 9.
  const nineIndex = zeroSixNine.findIndex((signal) =>
    four.every((segment) => signal.includes(segment))
  );
  const [nine] = zeroSixNine.splice(nineIndex, 1);
  // 0.
  const [zero] = zeroSixNine;
  // 2. 3. 5. signal length = 5;
  const twoThreeFive = signals.filter((signal) => signal.length === 5);
  // 3.
  const threeIndex = twoThreeFive.findIndex((signal) =>
    one.every((segment) => signal.includes(segment))
  );
  const [three] = twoThreeFive.splice(threeIndex, 1);
  // 5.
  const fiveIndex = twoThreeFive.findIndex(
    (signal) => six.filter((segment) => !signal.includes(segment)).length == 1
  );
  const [five] = twoThreeFive.splice(fiveIndex, 1);
  const [two] = twoThreeFive;

  const mapping = new Map([
    [seven.find((s) => !one.includes(s)), "a"],
    [nine.find((segment) => !three.includes(segment)), "b"],
    [two.find((segment) => !six.includes(segment)), "c"],
    [eight.find((segment) => !zero.includes(segment)), "d"],
    [zero.find((segment) => !nine.includes(segment)), "e"],
    [one.find((segment) => !two.includes(segment)), "f"],
    [nine.find((segment) => ![...four, ...seven].includes(segment)), "g"],
  ]);

  return parseInt(outputs
    .map((output) => {
      const mapped = output.map((letter) => mapping.get(letter));
      const mappedValue = segmentsToValue(mapped);
      const value = values.get(mappedValue);

      return value;
    })
    .join(""));
}

/**
 * Count unique outputs
 */
function countOutputs(segments: Pattern[]) {
  return segments.reduce(
    (count, segment) =>
      count +
      segment.outputs.filter((output) => ![5, 6].includes(output.length))
        .length,
    0
  );
}

export { countOutputs, decodeSignals };
