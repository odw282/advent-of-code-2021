import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import { grow, growMap } from "./index";

const testFish = [3,4,3,1,2];

const fish = readFileIntoArray(path.join(__dirname, "./input.txt"))[0].split(",").map(
  (value) => parseInt(value)
);
describe("--- Day 6: Lanternfish ---", () => {

    test("Test Case A", () => {
      const answer = grow([...testFish], 80)
      expect(answer).toEqual(5934);
    });

    test("Solution A", () => {
      const answer =  0//grow([...fish], 80);
      console.info("A:", answer);
    });

    test("Test Case B", () => {
      const answer = growMap([...testFish], 256)
      expect(answer).toEqual(26984457539);
    });

    it("Solution B", () => {
      const answer = growMap([...fish], 256)
      console.info("B:", answer);
    });

});
