/**
 * Naive version
 * @param fish
 * @param days
 */
function grow(fish: number[], nrDays: number, debug = false) {
  const generations = [`Initial state: ${fish.join(",")}`];
  for (let day = 1; day <= nrDays; day++) {
    for (let i = fish.length - 1; i >= 0; i--) {
      if (fish[i] === 0) {
        fish.push(8);
        fish[i] = 6;
      } else {
        fish[i]--;
      }
    }
    generations.push(
      `After ${day > 9 ? "" : " "}${day} day${
        day > 1 ? "s:" : ": "
      } ${fish.join(",")} = ${fish.length}`
    );
  }
  if (debug) console.log(generations.join("\n"));
  return fish.length;
}

/**
 * Optimized version
 * @param fish
 * @param days
 */
function growMap(fishes: number[], nrDays: number, debug = false) {
  const days = new Array(9).fill(0);
  for (const nrFish of fishes) {
    const fishCount = fishes.filter((fish) => fish === nrFish).length;
    days[nrFish] = fishCount;
  }
  const generations = [`Initial state: ${days.join(",")}`];

  // For each day shift the array
  for (let day = 1; day <= nrDays; day++) {
    // Age all the fish by one day
    const newFish = days.shift();

    // add new fish to back of the line
    days.push(newFish);

    // add existing fish back in action
    days[6] += newFish;

    generations.push(
      `After ${day > 9 ? "" : " "}${day} day${
        day > 1 ? "s:" : ": "
      } ${days.join(",")} = ${days.reduce((total, fish) => total + fish, 0)}`
    );
  }
  if (debug) console.log(generations.join("\n"));
  return days.reduce((total, fish) => total + fish, 0);
}

export { grow, growMap };
