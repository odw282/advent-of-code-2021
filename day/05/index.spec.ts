import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import { dangerWillRobinson } from "./index";

const mapToLine = (line: string) => {
  const [[x1, y1], [x2, y2]] = line
    .split(" -> ")
    .map((point) => point.split(",").map((i) => parseInt(i)));

  return {
    from: {
      x: x1,
      y: y1,
    },
    to: {
      x: x2,
      y: y2,
    },
  };
};

const testVentLines = readFileIntoArray(path.join(__dirname, "./test.txt")).map(
  mapToLine
);

const ventlines = readFileIntoArray(path.join(__dirname, "./input.txt")).map(
  mapToLine
);

describe("--- Day 5: Hydrothermal Venture ---", () => {
  test("Test Case A", () => {
    const answer = dangerWillRobinson(testVentLines);
    expect(answer).toEqual(5);
  });

  test("At how many points do at least two lines overlap?", () => {
    const answer = dangerWillRobinson(ventlines);
    console.info("A:", answer);
  });

  test("Test Case B", () => {
    const answer = dangerWillRobinson(testVentLines, false)
    expect(answer).toEqual(12);
  });

  it("At how many points do at least two lines overlap?", () => {
    const answer = dangerWillRobinson(ventlines, false)
    console.info("B:", answer);
  });
});
