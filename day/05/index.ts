export interface Point {
  x: number;
  y: number;
}

export interface Line {
  from: Point;
  to: Point;
}

/**
 * Find are the dangerous vent crossings
 */
function dangerWillRobinson(lines: Line[], noDiag = true) {
  // Use only horizontal / vertical lines
  if (noDiag) {
    lines = lines.filter(
      (line) => line.from.x === line.to.x || line.from.y === line.to.y
    );
  }

  // Find max size of field
  let { xMax, yMax } = lines.reduce(
    ({ xMax, yMax }, line) => {
      return {
        xMax: Math.max(xMax, Math.max(line.from.x, line.to.x)),
        yMax: Math.max(yMax, Math.max(line.from.y, line.to.y)),
      };
    },
    { xMax: 0, yMax: 0 }
  );
  xMax += 1;
  yMax += 1;

  // Init empty field
  const field = initField(xMax, yMax);

  // For each line update field
  // prettier-ignore
  for (let i = 0; i < lines.length; i++) {
    const line = lines[i];
    for (let x = Math.min(line.from.x, line.to.x); x <= Math.max(line.from.x, line.to.x); x++) {
      for (let y = Math.min(line.from.y, line.to.y); y <= Math.max(line.from.y, line.to.y); y++) {
        // Test if point is on line
        if ((x - line.from.x) * (line.to.y - line.from.y) - (y - line.from.y) * (line.to.x - line.from.x) === 0) {
          field[x + (y * xMax)] += 1;
        }
      }
    }
  }
  return field.filter((i) => i >= 2).length;
}

/**
 * Init field
 * @param xMax
 * @param yMax
 */
function initField(xMax: number, yMax: number) {
  const field = [];
  for (let x = 0; x < xMax; x++) {
    for (let y = 0; y < yMax; y++) {
      field[x + y * xMax] = 0;
    }
  }
  return field;
}

/**
 * Print field
 * @param field
 * @param xMax
 * @param yMax
 */
function printField(field: number[], xMax: number, yMax: number) {
  const print = [];
  for (let y = 0; y < yMax; y++) {
    print.push(
      field
        .slice(y * yMax, xMax + y * yMax)
        .map((i) => (i === 0 ? "." : i))
        .join("")
    );
  }
  console.log(print.join("\n"));
}

export { dangerWillRobinson };
