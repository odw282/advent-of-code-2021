
function gammaEpsilon(lines: number[], bits: number) {
  let most = 0;
  // for each bit position we count the amount of 1s
  for (let i = 0; i < bits; i++) {
    let count = 0;
    for (let line of lines) {
      // if the line has a 1 at the i position we count it
      if (line & (1 << i)) {
        count++;
      }
      // We reached the threshold and set this bit (value)
      // by or-ing it with the existing value
      if (count > lines.length / 2) {
        most |= 1 << i;
        break;
      }
    }
  }
  // we OR the most common bits with 1s to get the inverse
  return most * (most ^ parseInt(Array(bits).fill(1).join(""), 2));
}

function findRating(lines: number[], bits: number) {
  let oxygenValue = 0;
  let co2Value = 0;
  let mask = 0;
  // For each bit we count the amount
  for (let i = bits - 1; i >= 0; i--) {
    let oxygenCount = 0;
    let co2Count = 0;
    let oxygenTotal = 0;
    let co2Total = 0;

    // for each line find the most and least common bits
    for (let line of lines) {
      // We OR the line with the prefix and mask out the bits we are not looking at yet
      if (((line ^ oxygenValue) & mask) === 0) {
        // if the line has a 1 at the i position we count it
        if (line & (1 << i)) {
          oxygenCount++;
        }
        oxygenTotal++;
      }
      // We OR the line with the prefix and mask out the bits we are not looking at yet
      if (((line ^ co2Value) & mask) === 0) {
        // if the line has a 1 at the i position we count it
        if (line & (1 << i)) {
          co2Count++;
        }
        co2Total++;
      }
    }

    // if only one value remains we can use it to update our prefix safely
    if (oxygenTotal === 1) {
      oxygenValue |= oxygenCount << i;

      // If 1s are the most common update prefix to include it at the i position
    } else if (oxygenCount >= oxygenTotal / 2) {
      oxygenValue |= 1 << i;
    }
    if (co2Total === 1) {
      co2Value |= co2Count << i;

      // If 1s are the least common update prefix to include it at the i position
    } else if (co2Count < co2Total / 2) {
      co2Value |= 1 << i;
    }
    // expand the mask to include more bits in the count
    mask |= 1 << i;
  }
  return oxygenValue * co2Value;
}

export { gammaEpsilon, findRating };
