import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import { findRating, gammaEpsilon } from "./index";

const testReport = [
  "00100",
  "11110",
  "10110",
  "10111",
  "10101",
  "01111",
  "00111",
  "11100",
  "10000",
  "11001",
  "00010",
  "01010",
].map((line) => parseInt(line, 2));

const reports = readFileIntoArray(path.join(__dirname, "./input.txt")).map(
  (line) => parseInt(line, 2)
);

describe("--- Day 3: Binary Diagnostic ---", () => {
  test("Test Case A", () => {
    const answer = gammaEpsilon(testReport, 5);
    expect(answer).toEqual(198);
  });

  test("What is the power consumption of the submarine?", () => {
    const answer = gammaEpsilon(reports, 12);
    console.info("A:", answer);
  });

  test("Test Case B", () => {
    const answer = findRating(testReport, 5)
    expect(answer).toEqual(230);
  });

  it("What is the life support rating of the submarine?", () => {
    const answer = findRating(reports, 12)
    console.info("B:", answer);
  });
});
