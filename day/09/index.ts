export interface Node {
  x: number;
  y: number;
  index: number;
  height: number;
  top?: Node | null;
  left?: Node | null;
  right?: Node | null;
  bottom?: Node | null;
}

/**
 * Create node adjacently map
 * @param map
 */
function mapToNodes(map: number[][]) {
  const nodes = new Map<number, Node>();
  // Build nodes map
  for (let y = 0; y < map.length; y++) {
    for (let x = 0; x < map[y].length; x++) {
      const index = x + y * map[y].length;
      nodes.set(index, {
        x,
        y,
        index,
        height: map[y][x],
      });
    }
  }

  // Link nodes
  for (let y = 0; y < map.length; y++) {
    for (let x = 0; x < map[y].length; x++) {
      const index = x + y * map[y].length;
      const node = nodes.get(index);
      node.top = y > 0 ? nodes.get(index - map[y].length) : null;
      node.bottom =
        y < map.length - 1 ? nodes.get(index + map[y].length) : null;
      node.left = x > 0 ? nodes.get(index - 1) : null;
      node.right = x < map[y].length - 1 ? nodes.get(index + 1) : null;
    }
  }
  return nodes;
}

/**
 * Is lower that than all the neighbours
 * @param node
 */
const isLowest = (node: Node) =>
  (node.top === null || node.height < node.top.height) &&
  (node.bottom === null || node.height < node.bottom.height) &&
  (node.left === null || node.height < node.left.height) &&
  (node.right === null || node.height < node.right.height);

/**
 * Calculate risk level from Low Points
 * @param map {number[][]}
 */
function lowPoints(map: number[][]) {
  // Create node adjacently map
  const nodes = mapToNodes(map);

  // Calculate risk level
  const riskLevel = (node: Node) => (isLowest(node) ? node.height + 1 : 0);
  // sum risk level
  return Array.from(nodes.values()).reduce(
    (risk, node) => risk + riskLevel(node),
    0
  );
}

/**
 * Recursive function that visits all adjacent nodes and adds them if height < 9
 * @param start
 */
const addToBasin = (start: Node) => {
  const visited = new Set<Node>();
  const basin = new Set<Node>();

  // Add to basin
  const add = (to: Node) => {
    // Add node to set
    basin.add(to);

    // Add to visited
    visited.add(to);

    ["top", "bottom", "left", "right"].forEach((direction) => {
      // Node exists and has not been visited yet
      // @ts-ignore
      if (to[direction] && !visited.has(to[direction])) {
        // Add node to visited list
        // @ts-ignore
        visited.add(to[direction]);

        // Node is lower that MAX node height
        // @ts-ignore
        if (to[direction].height < 9) {
          // @ts-ignore
          add(to[direction]);
        }
      }
    });
  };
  add(start);
  return basin;
};

/**
 * Find basin
 * @param map
 */
function findBasin(map: number[][]) {
  return Array.from(mapToNodes(map).values())
    .filter((node) => isLowest(node))
    .map((node) => addToBasin(node))
    .map((basin) => basin.size)
    .sort((a, b) => a - b)
    .slice(-3)
    .reduce((sum, count) => sum * count, 1);
}

export { lowPoints, findBasin };
