import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import { findBasin, lowPoints } from "./index";

const testMap = readFileIntoArray(path.join(__dirname, "./test.txt")).map(
  (row) => row.split("").map(value => parseInt(value))
);

const map = readFileIntoArray(path.join(__dirname, "./input.txt")).map(
  (row) => row.split("").map(value => parseInt(value))
);

describe("--- Day 9: Smoke Basin ---", () => {

    test("Test Case A", () => {
      const answer = lowPoints(testMap);
      expect(answer).toEqual(15);
    });

    test("What is the sum of the risk levels of all low points on your heightmap?", () => {
      const answer = lowPoints(map);
      console.info("A:", answer);
    });

    test("Test Case B", () => {
      const answer = findBasin(testMap);
      expect(answer).toEqual(1134);
    });

    it("What do you get if you multiply together the sizes of the three largest basins?", () => {
      const answer = findBasin(map);
      console.info("B:", answer);
    });

});
