import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import { countIncreases, countSlidingWindowIncreases } from "./index";
const testMeasurements = [199, 200, 208, 210, 200, 207, 240, 269, 260, 263];
const measurements = readFileIntoArray(path.join(__dirname, "./input.txt")).map(
  (value) => parseInt(value)
);
describe("--- Day 1: Sonar Sweep ---", () => {
  test("Test Case A", () => {
    const answer = countIncreases(testMeasurements);
    expect(answer).toEqual(7);
  });

  test("How many measurements are larger than the previous measurement?", () => {
    const answer = countIncreases(measurements);
    console.info("A:", answer);
  });

  test("Test Case B", () => {
    const answer = countSlidingWindowIncreases(testMeasurements);
    expect(answer).toEqual(5);
  });

  it("How many sums are larger than the previous sum?", () => {
    const answer = countSlidingWindowIncreases(measurements);
    console.info("B:", answer);
  });
});
