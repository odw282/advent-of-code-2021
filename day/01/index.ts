/**
 * Counts the number of times the depth increases
 * @param measurements
 */
function countIncreases(measurements: number[]) {
  return measurements.reduce((increases, currentDepth, index, arr) => {
    if (arr[index-1] !== null && currentDepth > arr[index-1]) {
      increases++;
    }
    return increases;
  }, 0);
}

/**
 * Counts increases of a sliding window
 * @param measurements
 * @param window
 */
function countSlidingWindowIncreases(measurements: number[], window = 3) {
  const windowMaxIndex = Math.max(1, window - 1);
  const sums = measurements.reduce((sums, depth, index, arr) => {
    if (arr[index + windowMaxIndex]) {
      sums.push(
        depth + arr[index + windowMaxIndex - 1] + arr[index + windowMaxIndex]
      );
    }
    return sums;
  }, []);
  return countIncreases(sums);
}

export { countIncreases, countSlidingWindowIncreases };
