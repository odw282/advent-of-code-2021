export type Direction = "forward" | "up" | "down";

export interface Command {
  direction: Direction;
  amount: number;
}

/**
 * Dive the submarine
 * @param commands
 */
function dive(commands: Command[]) {
  let depth = 0,
    position = 0;
  commands.forEach((command) => {
    switch (command.direction) {
      case "up":
        depth -= command.amount;
        break;
      case "down":
        depth += command.amount;
        break;
      case "forward":
        position += command.amount;
        break;
    }
  });
  return position * depth;
}

/**
 * Aim the submarine
 * @param commands
 */
function aim(commands: Command[]) {
  let depth = 0,
    position = 0,
    aim = 0;
  commands.forEach((command) => {
    switch (command.direction) {
      case "up":
        aim -= Math.max(0, command.amount);
        break;
      case "down":
        aim += command.amount;
        break;
      case "forward":
        position += command.amount;
        depth += aim * command.amount;
        break;
    }
  });
  return position * depth;
}

export { dive, aim };
