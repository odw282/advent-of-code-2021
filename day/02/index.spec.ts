import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import { aim, Command, Direction, dive } from "./index";

const testCommands = [
  "forward 5",
  "down 5",
  "forward 8",
  "up 3",
  "down 8",
  "forward 2",
];

const parseCommands = (line: string): Command => {
  const [direction, amount] = line.split(" ");
  return {
    direction: direction as Direction,
    amount: parseInt(amount),
  };
};

const commands = readFileIntoArray(path.join(__dirname, "./input.txt")).map(
  parseCommands
);

describe("--- Day 2: Dive! ---", () => {
  test("Test Case A", () => {
    const commands = testCommands.map(parseCommands);
    const answer = dive(commands);
    expect(answer).toEqual(150);
  });

  test("What do you get if you multiply your final horizontal position by your final depth?", () => {
    const answer = dive(commands);
    console.info("A:", answer);
  });

  test("Test Case B", () => {
    const commands = testCommands.map(parseCommands);
    const answer = aim(commands);
    expect(answer).toEqual(900);
  });

  it("What do you get if you multiply your final horizontal position by your final depth?", () => {
    const answer = aim(commands);
    console.info("B:", answer);
  });
});
