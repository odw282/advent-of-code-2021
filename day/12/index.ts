export type CaveSystem = Map<string, Cave>;

export interface Cave {
  id: String;
  small: boolean;
  links: Cave[];
}

export const START = "start";
export const END = "end";

/**
 * Build cave system
 * @param paths
 */
function makeSystem(paths: string[]) {
  const system = new Map<string, Cave>();
  // Make list of all caves
  paths.forEach((path) => {
    const [from, to] = path.split("-");
    if (!system.has(to)) {
      system.set(to, {
        id: to,
        small: to === to.toLowerCase(),
        links: [],
      });
    }
    if (!system.has(from)) {
      system.set(from, {
        id: from,
        small: from === from.toLowerCase(),
        links: [],
      });
    }
  });

  // Link all the caves
  paths.forEach((path) => {
    const [from, to] = path.split("-");
    system.get(to).links.push(system.get(from));
    system.get(from).links.push(system.get(to));
  });
  return system;
}

/**
 * smallCaveTwicePaths
 */
function smallCaveTwicePaths(system: CaveSystem) {
  const start = system.get(START);
  const end = system.get(END);

  // List of all paths
  const paths: Cave[][] = [];

  // Visit a cave
  const visit = (cave: Cave, path: Cave[], once: Cave[], twice: Cave[]) => {
    // Add cave to path
    path.push(cave);

    // end of path
    if (cave === end) {
      return path;
    }

    // visit all the links, each visit should create a completely new path
    cave.links.forEach((link) => {
      // Large cave can be visited multiple times
      // Small cave can only be visited once in a single path run or twice for a single cave
      if (link.small) {
        // Make sure we visit a cave at least once
        if (!once.includes(link)) {
          paths.push(visit(link, [...path], [...once, link], [...twice]));
        }
        // And one of the caves we visit at most twice
        if (!twice.includes(link) && twice.length < 3) {
          paths.push(visit(link, [...path], [...once, link], [...twice, link]));
        }
      } else {
        // and get a new list of routes
        paths.push(visit(link, [...path], [...once], [...twice]));
      }
    });

    return path;
  };

  // Start visit of cave system, make sure to include start and end as caves visited twice already
  visit(start, [], [start], [start, end]);

  // Filter out paths that did not include the end node.
  // Filter out duplicates
  return paths
    .filter((path) => path.includes(end))
    .map((path) => path.map((cave) => cave.id).join(","))
    .reduce((unique, path) => {
      if (!unique.includes(path)) {
        unique.push(path);
      }
      return unique;
    }, []).length;
}

/**
 * smallCaveOncePaths
 */
function smallCaveOncePaths(system: CaveSystem) {
  const start = system.get(START);
  const end = system.get(END);

  // List of all paths
  const paths: Cave[][] = [];

  // Visit a cave
  const visit = (cave: Cave, path: Cave[], visited: Cave[]) => {
    // Add cave to path
    path.push(cave);

    // end of path
    if (cave === end) {
      return path;
    }

    // visit all the links, each visit should create a completely new path
    cave.links.forEach((link) => {
      // Large cave can be visited multiple times
      if (link.small) {
        if (!visited.includes(link)) {
          // Small cave can only be visited once in a single path run
          paths.push(visit(link, [...path], [...visited, link]));
        }
      } else {
        // and get a new list of routes
        paths.push(visit(link, [...path], [...visited]));
      }
    });

    return path;
  };

  // Start visit of cave system
  visit(start, [], [start]);

  // Filter out paths that did not include the end node.
  return paths.filter((path) => path.includes(end)).length;
}

export { smallCaveOncePaths, smallCaveTwicePaths, makeSystem };
