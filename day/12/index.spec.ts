import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import { smallCaveOncePaths, smallCaveTwicePaths, makeSystem } from "./index";

const testSmall = makeSystem(readFileIntoArray(path.join(__dirname, "./test-small.txt")))
const testMedium = makeSystem(readFileIntoArray(path.join(__dirname, "./test-medium.txt")))
const testLarge = makeSystem(readFileIntoArray(path.join(__dirname, "./test-large.txt")))
const caves = makeSystem(readFileIntoArray(path.join(__dirname, "./input.txt")))

describe("--- Day 12: Passage Pathing ---", () => {

    test("Test Case A - small", () => {
      const answer = smallCaveOncePaths(testSmall);
      expect(answer).toEqual(10);
    });

    test("Test Case A - medium", () => {
      const answer = smallCaveOncePaths(testMedium);
      expect(answer).toEqual(19);
    });

    test("Test Case A - large", () => {
      const answer = smallCaveOncePaths(testLarge);
      expect(answer).toEqual(226);
    });

    test("How many paths through this cave system are there that visit small caves at most once?", () => {
      const answer = smallCaveOncePaths(caves)
      console.info("A:", answer);
    });

    test("Test Case B", () => {
      const answer = smallCaveTwicePaths(testSmall);
      expect(answer).toEqual(36);
    });

    it("How many paths through this cave system are there?", () => {
      const answer =  smallCaveTwicePaths(caves);
      console.info("B:", answer);
    });

});
