export type CompareFn = (a: number, b: number) => number;

const cache = new Map();
function triangle(n: number) {
  if (cache.has(n)) {
    return cache.get(n);
  }
  let sum = (n * (n + 1)) / 2;
  cache.set(n, sum);
  return sum;
}

/**
 *
 * @param crabs
 */
function calculateFuelMedian(crabs: number[]) {
  crabs.sort((a: number, b: number) => a - b);
  const mid = Math.floor(crabs.length / 2);
  const median =
    crabs.length % 2 ? crabs[mid] : (crabs[mid - 1] + crabs[mid]) / 2;
  const fuel = crabs.map((distance) => Math.abs(distance - median));
  return fuel.reduce((sum, fuel) => sum + fuel, 0);
}

/**
 * Use average to
 * @param crabs
 */
function calculateFuelAverage(crabs: number[]) {
  crabs.sort((a: number, b: number) => triangle(a) - triangle(b));
  const average =
    crabs.reduce((sum, distance) => sum + distance, 0) / crabs.length;
  const fuel = crabs.map((distance) =>
    triangle(Math.abs(distance - Math.floor(average)))
  );
  return fuel.reduce((sum, fuel) => sum + fuel, 0);
}

export { calculateFuelMedian, calculateFuelAverage };
