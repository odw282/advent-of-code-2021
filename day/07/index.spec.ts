import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import { calculateFuelAverage, calculateFuelMedian } from "./index";

const testCrabs = [16,1,2,0,4,2,7,1,2,14];

const crabs = readFileIntoArray(path.join(__dirname, "./input.txt"))[0].split(",").map(
  (value) => parseInt(value)
);
describe("--- Day 7: The Treachery of Whales ---", () => {

    test("Test Case A", () => {
      const answer = calculateFuelMedian(testCrabs);
      expect(answer).toEqual(37);
    });

    test("How much fuel must they spend to align to that position?", () => {
      const answer = calculateFuelMedian(crabs);
      console.info("A:", answer);
    });

    test("Test Case B", () => {
      const answer = calculateFuelAverage(testCrabs);
      expect(answer).toEqual(170);
    });

    it("How much fuel must they spend to align to that position?", () => {
      const answer = calculateFuelAverage(crabs);
      console.info("B:", answer);
    });

});
