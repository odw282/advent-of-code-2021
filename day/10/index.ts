const POINTS = new Map<string | boolean, number>([
  [")", 3],
  ["]", 57],
  ["}", 1197],
  [">", 25137],
  [false, 0],
]);

const COMPLETION_POINTS = new Map<string, number>([
  [")", 1],
  ["]", 2],
  ["}", 3],
  [">", 4],
]);

const OPEN = ["(", "{", "[", "<"];
const CLOSE = [")", "}", "]", ">"];

const isOpen = (char: string) => OPEN.includes(char);
const isClose = (char: string) => CLOSE.includes(char);
const getClose = (char: string) => CLOSE[OPEN.findIndex((c) => c === char)];
const getOpen = (char: string) => OPEN[CLOSE.findIndex((c) => c === char)];

function findClosingChars(line: string) {
  // List of close tags
  let closing: string[] = [];

  // List of missing tags
  let missing: string[] = [];

  // Create open en close lists
  line
    .split("")
    .reverse()
    .map((char, index) => {
      // For a open char just add it to the open list
      if (isOpen(char)) {
        if (closing.length && closing[closing.length - 1] === getClose(char)) {
          closing.pop();
          return char;
        } else {
          missing.push(getClose(char));
          return getClose(char);
        }
      }

      // For a close char we just add it to the list
      if (isClose(char)) {
        closing.push(char);
        return char;
      }
    })
    .reverse();

  return missing;
}

/**
 * Find all closing errors
 * @param line
 */
function findClosingErrors(line: string) {
  // List of open tags
  let opening: string[] = [];

  // Look at each char
  const errors = line.split("").map((char, index) => {
    // A closing char means we need to look if it matches the last opening char
    if (isClose(char)) {
      //
      if (opening.length && opening[opening.length - 1] === getOpen(char)) {
        opening.pop();
        return false;
      } else {
        return char;
      }
    }

    // We add opening char to the end of the opening list
    if (isOpen(char)) {
      opening.push(char);
      return false;
    }

    // Return error
    return true;
  });

  return errors;
}

/**
 * Calculate completion score
 * @param lines
 */
function completionScore(lines: string[]) {
  const points = lines
    .filter((line) => findClosingErrors(line).every((e) => e === false))
    .map((line) => findClosingChars(line))
    .map((missing) =>
      missing.reduce(
        (total, char) => total * 5 + COMPLETION_POINTS.get(char),
        0
      )
    )
    .sort((a, b) => a - b);
  return points[Math.floor(points.length / 2)];
}

/**
 * Calculate error score
 * @param lines
 */
function errorScore(lines: string[]) {
  const points = lines
    .map((line) => findClosingErrors(line))
    .filter((error) => !error.every((e) => e === false))
    .map((error) => error.find((e) => e !== false))
    .map((char) => POINTS.get(char));
  return Array.from(POINTS.values()).reduce(
    (total, point, index) =>
      total + point * points.filter((p) => p === point).length,
    0
  );
}

export { errorScore, completionScore };
