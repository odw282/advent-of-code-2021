import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import { completionScore, errorScore } from "./index";

const navTest = readFileIntoArray(path.join(__dirname, "./test.txt"));

const nav = readFileIntoArray(path.join(__dirname, "./input.txt"));

describe("--- Day 10: Syntax Scoring ---", () => {

    test("Test Case A", () => {
      const answer = errorScore(navTest);
      expect(answer).toEqual(26397);
    });

    test("What is the total syntax error score for those errors?", () => {
      const answer = errorScore(nav);
      console.info("A:", answer);
    });

    test("Test Case B", () => {
      const answer = completionScore(navTest);
      expect(answer).toEqual(288957);
    });

    it("Solution B", () => {
      const answer = completionScore(nav);
      console.info("B:", answer);
    });

});
