import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";

const amounts = readFileIntoArray(path.join(__dirname, "./input.txt")).map(
  (value) => parseInt(value)
);
describe("Day X: Title", () => {

    test("Test Case A", () => {
      const answer = "A";
      expect(answer).toEqual("A");
    });

    test("Solution A", () => {
      const answer = "A";
      expect(answer).toEqual("A");
      console.info("A:", answer);
    });

    test("Test Case B", () => {
      const answer = "A";
      expect(answer).toEqual("A");
    });

    it("Solution B", () => {
      const answer = "B";
      expect(answer).toEqual("B");
      console.info("B:", answer);
    });

});
